//
//  Message.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 21/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase



/* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
// ------------- Создаем класс Сообщение ----------------//
// -- в объекты класса будем передавать данные полученны //
// ------------------ из Firebase -----------------------//



class Message: NSObject {
    
    var fromId     : String?
    var text       : String?
    var timestamp  : NSNumber? //не хочет работать с Int! беря данные из Firebase
    var toId       : String?
    
    var imageUrl   : String?
    var imageHeight: NSNumber?
    var imageWidth : NSNumber?
    
    var videoUrl   : String?
    
    func chatPartnerId() -> String? {
        
        return fromId == Auth.auth().currentUser?.uid ? toId : fromId

    }
    
    init(dictionary: [String:Any]) {
        super.init()
        
        fromId      = dictionary["fromId"]      as? String
        text        = dictionary["text"]        as? String
        timestamp   = dictionary["timestamp"]   as? NSNumber
        toId        = dictionary["toId"]        as? String
        
        imageUrl    = dictionary["imageUrl"]    as? String
        imageHeight = dictionary["imageHeight"] as? NSNumber
        imageWidth  = dictionary["imageWidth"]  as? NSNumber
        
        videoUrl    = dictionary["videoUrl"]    as? String
    }
    
}



/* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
