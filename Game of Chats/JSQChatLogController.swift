//
//  JSQChatLogController.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 02/10/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Firebase
import MobileCoreServices
import AVFoundation
import AVKit

class JSQChatLogController: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    private var jsqMessages = [JSQMessage]()
    var messages = [Message]()
    
    
    
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
    
    var startingFrame: CGRect?
    var blackBackgroundView: UIView?
    var startingImageView: UIImageView?
    
    var user: User? {
        didSet { // --- как только получили значение имени, ставим в заголовок
            navigationItem.title = user?.name
            setSenderIdAndName()
        } // ---
    }
    
    let imagePickerController = UIImagePickerController()
    
    
    func setSenderIdAndName() {
        
        //=== обязательно сообщаем JSQMessagesViewController,
        //=== кто является текущим пользователем
        
        self.senderId = ""
        self.senderDisplayName = ""
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: Any] {
                
                if let name = dictionary["name"] as? String {
                    self.senderDisplayName = name
                    self.senderId = uid
                }
            }
        })
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        observeMessages()

    }
    

    

    //НАЧАЛО : === РЕАЛИЗОВЫВАЕМ ОБЯЗАТЕЛЬНЫЕ МЕТОДЫ JSQMessagesViewController ===
    //--- начало: возвращаем количество сообщений
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return jsqMessages.count
        
    }//--- конец: возвращаем количество сообщений
    
    
    
    
    
    //--- начало: возвращаем содержимое сообщений
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return jsqMessages[indexPath.row]
        
    }//--- конец: возвращаем содержимое сообщений
    
    
    
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        return cell
    }
    
    
    
    
    //--- начало: задаем цвета сообщений в диалоге
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        let bubleFactory = JSQMessagesBubbleImageFactory()
        let message = jsqMessages[indexPath.row]
        if self.senderId == message.senderId {
            return bubleFactory?.outgoingMessagesBubbleImage(with: UIColor.green)
        } else {
            return bubleFactory?.incomingMessagesBubbleImage(with: UIColor.blue)
        }
        
    }//--- конец: задаем цвета сообщений в диалоге
    

    
    //--- начало: устанавливаем аватарки
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        guard let profileImageUrl = self.user?.profileImageURL else { return nil }
        
        let imageView = UIImageView()
        imageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        
        let avatar = JSQMessagesAvatarImageFactory.avatarImage(with: imageView.image, diameter: 10)
        
        return avatar
        
    }//--- конец: устанавливаем аватарки
    
 
    
    
    //--- начало: устанавливаем подписи к сообщения (от кого:)
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        let message = jsqMessages[indexPath.row]
        let messageUsername = message.senderDisplayName
        return NSAttributedString(string: messageUsername!)
        
    }//--- конец: устанавливаем подписи к сообщения (от кого:)
    
    
    
    
    
    //--- начало: устанавливаем высоту "облачков" сообщений
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
        
    }//--- конец: устанавливаем высоту "облачков" сообщений
    
    
    
    //--- начало: для проигрывания видео
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        
        let message = jsqMessages[indexPath.item]
        
        if message.isMediaMessage {
            if let mediaItem = message.media as? JSQVideoMediaItem {
                let player = AVPlayer(url: mediaItem.fileURL)
                let playerController = AVPlayerViewController()
                playerController.player = player
                self.present(playerController, animated: true, completion: nil)
            }
            if let mediaItem = message.media as? JSQPhotoMediaItem {
                let imageView = UIImageView(image: mediaItem.image)
                imageView.isUserInteractionEnabled = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
            }
        }
        
    }
    
    //--- конец: для проигрывания видео
    
    
    
    //--- начало: отправка исходящих сообщений
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        //guard let message = JSQMessage(senderId: senderId, displayName: senderDisplayName, text: text) else { return }
        //messages.append(message)
        
        
        
//        let ref = Database.database().reference().child("messages")
//        let childRef = ref.childByAutoId()
//        guard let toId = user?.id else { return }
//        //let fromId = Auth.auth().currentUser!.uid
//        let timestamp = UInt64(Date().timeIntervalSince1970)
//        
//        var values = ["toId"       : toId,
//                      "fromId"     : senderId,
//                      "text"       : text,
//                      "timestamp"  : timestamp] as [String : Any]
//        
//        childRef.updateChildValues(values) { (error, ref) in
//            if error != nil {
//                print(error)
//                return
//            }
//            
//            let userMessageRef = Database.database().reference().child("user-messages").child(senderId).child(toId)
//            let messageId = childRef.key
//            userMessageRef.updateChildValues([messageId: 1])
//            
//            let recipientUserMessagesRef = Database.database().reference().child("user-messages").child(toId).child(senderId)
//            recipientUserMessagesRef.updateChildValues([messageId: 1])
//            
//        }
        
        handleSend(text: text)
        
        finishSendingMessage()
        
        collectionView.reloadData()
       
    }//--- конец: отправка исходящих сообщений
    

    
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        self.inputToolbar.contentView!.textView!.resignFirstResponder()
        
        let sheet = UIAlertController(title: "Media messages", message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Send photo", style: .default) { (action) in
            /**
             *  Create fake photo
             */
            self.handleUploadTap(type: kUTTypeImage)
            
            
        }
        
//        let locationAction = UIAlertAction(title: "Send location", style: .default) { (action) in
//            /**
//             *  Add fake location
//             */
//            let locationItem = self.buildLocationItem()
//            
//            self.addMedia(locationItem)
//        }
//        
        let videoAction = UIAlertAction(title: "Send video", style: .default) { (action) in
            /**
             *  Add fake video
             */
            self.handleUploadTap(type: kUTTypeMovie)

        }
//
//        let audioAction = UIAlertAction(title: "Send audio", style: .default) { (action) in
//            /**
//             *  Add fake audio
//             */
//            let audioItem = self.buildAudioItem()
//            
//            self.addMedia(audioItem)
//        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        sheet.addAction(photoAction)
//        sheet.addAction(locationAction)
        sheet.addAction(videoAction)
//        sheet.addAction(audioAction)
        sheet.addAction(cancelAction)
        
        self.present(sheet, animated: true, completion: nil)
    }

    
    
    //MARK: Photo attaching
    
    private func handleImageSelectedForInfo(info: [String: Any]) { //---старт: выбор картинки из галереии
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            print("edited image")
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print("orirginal image")
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            print("image chosen")
            uploadToFirebaseStorageUsingImage(image: selectedImage, completion: { (imageUrl) in
                self.sendMessageWithImageUrl(imageUrl: imageUrl, image: selectedImage)
            })
        }
        
    } //---конец: выбор картинки из галереии
    
    
    
    func handleSend(text: String) { //-- start:  отправка сообщения пользователю
        
        let properties = ["text": text] as [String : Any]
        sendMessageWithProperties(properties: properties)
        
        
    } //-- end:  отправка сообщения пользователю
    
    
    
    private func sendMessageWithImageUrl(imageUrl: String, image: UIImage) {
        
        let properties = ["imageUrl"   : imageUrl,
                          "imageWidth" : image.size.width,
                          "imageHeight": image.size.height] as [String : Any]
        
        sendMessageWithProperties(properties: properties)
        
    }
    
    
    
    
    private func sendMessageWithProperties(properties: [String: Any]) {
        
        let ref = Database.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        let toId = user!.id!
        let fromId = Auth.auth().currentUser!.uid
        let timestamp = UInt64(Date().timeIntervalSince1970)
        
        var values = ["toId"       : toId,
                      "fromId"     : fromId,
                      "timestamp"  : timestamp] as [String : Any]
        
        properties.forEach {values[$0] = $1}
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print(error)
                return
            }
            
            let userMessageRef = Database.database().reference().child("user-messages").child(fromId).child(toId)
            let messageId = childRef.key
            userMessageRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = Database.database().reference().child("user-messages").child(toId).child(fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
    }
    
    
    
    private func handleVideoSelectedForUrl(url: URL) { // start: когда выбрано видео
        
        let filename = UUID().uuidString
        let uploadTask = Storage.storage().reference().child("message_movies").child(filename).putFile(from: url, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed: ", error)
                return
            }
            
            if let videoUrl = metadata?.downloadURL()?.absoluteString {
                
                if let thumbnailImage = self.thumbnailImageForFileUrl(fileUrl: url) {
                    
                    self.uploadToFirebaseStorageUsingImage(image: thumbnailImage, completion: { (imageUrl) in
                        
                        let properties = ["imageUrl"   : imageUrl,
                                          "imageWidth" : thumbnailImage.size.width,
                                          "imageHeight": thumbnailImage.size.height,
                                          "videoUrl"   : videoUrl] as [String : Any]
                        
                            self.sendMessageWithProperties(properties: properties)
                        
                        
                    })
                }
            }
        })
        

    } // end: когда выбрано видео
    
    
    private func uploadToFirebaseStorageUsingImage(image: UIImage, completion: @escaping (_ imageUrl: String) -> ()) { //--- start: uploading images to FB
        let imageName = UUID().uuidString
        let ref = Storage.storage().reference().child("message_images").child(imageName)
        
        if let uploadData = UIImageJPEGRepresentation(image, 0.5) as? Data {
            
            ref.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    print("Failed to upload the image", error)
                    return
                    
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    
                    completion(imageUrl)
                    
                }
                
            })
        }
        
    }//--- end: uploading images to FB
    

    
    
    private func thumbnailImageForFileUrl(fileUrl: URL) -> UIImage? { //--- start: making the thumbnail from the video
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
            
        } catch let err {
            
            print(err)
            
        }
        
        return nil
    }//--- end: making the thumbnail from the video
    
    
    

    
    func handleUploadTap(type: CFString) { // --- start: выбор мультимедиа
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        //imagePickerController.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        imagePickerController.mediaTypes = [type as String]
        present(imagePickerController, animated: true, completion: nil)
        
    } // --- end: выбор мультимедиа
    
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) { // --- start: ставим фотку в профиль после выбора фотки из галереии
        
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            
            handleVideoSelectedForUrl(url: videoUrl)
            
            let video = JSQVideoMediaItem(fileURL: videoUrl, isReadyToPlay: true)
            self.jsqMessages.append(JSQMessage(senderId: senderId, displayName: senderDisplayName, media: video))
            
        } else if let picture = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            handleImageSelectedForInfo(info: info)
            
            let image = JSQPhotoMediaItem(image: picture)
            self.jsqMessages.append(JSQMessage(senderId: senderId, displayName: senderDisplayName, media: image))
            
        }
        
        // --- обязательно делаем дисмисс после выбора,
        // --- иначе экран с галереей не закроется
        dismiss(animated: true, completion: nil)
        collectionView.reloadData()
        
    } // --- end: ставим фотку в профиль после выбора фотки из галереии

    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
//    func addMedia(_ media:JSQMediaItem) {
//        let message = JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, media: media)
//        self.messages.append(message!)
//        
//        //Optional: play sent sound
//        
//        self.finishSendingMessage(animated: true)
//    }
    
    
    
    
    
    
    func observeMessages() { // ---  старт: получим сообщения чтобы  потом наполнять collection view
        
        guard let uid = Auth.auth().currentUser?.uid, let toId = user?.id else {
            return
        }
        
        let userMessagesRef = Database.database().reference().child("user-messages").child(uid).child(toId)
        
        userMessagesRef.observe(.childAdded, with: { (snapshot) in
            
            let messageId = snapshot.key
            
            let messageRef = Database.database().reference().child("messages").child(messageId)
            
            messageRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dictionary = snapshot.value as? [String: Any] {
                    let message = Message(dictionary: dictionary)
                    self.messages.append(message)
                    
                    if message.text != nil {
                        
                        if let id = message.fromId as? String {
                            
                            let senderName: String
                            
                            if id == self.user?.id {
                                senderName = (self.user?.name)!
                            } else {
                                senderName = self.senderDisplayName
                            }
                            
                            if let textMessage = JSQMessage(senderId: id, displayName: senderName, text: message.text) {
                                
                                self.jsqMessages.append(textMessage)
                                
                            }
                        
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
            }, withCancel: nil)
            
        }, withCancel: nil)
        
    } // --- конец сетевой функции


    private func populateMessagesToView() {
        
        for message in messages {
            let text = message.text
            
            
            
            
            
            
        }
        
        
        
    }
    
    
    //КОНЕЦ : === РЕАЛИЗОВЫВАЕМ ОБЯЗАТЕЛЬНЫЕ МЕТОДЫ JSQMessagesViewController ===
    
    
    
    
    func handleZoomTap(tapGesture: UITapGestureRecognizer) {
        
        if let imageView = tapGesture.view as? UIImageView {
            performZoomInForStartingImageView(startingImageView: imageView)
            
        }
    }

    
    func performZoomInForStartingImageView (startingImageView: UIImageView) { // --- start: увеличиваем картинку в чате при нажатии
        
        self.startingImageView = startingImageView
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        if let keyWindow = UIApplication.shared.keyWindow {
            
            blackBackgroundView = UIView(frame: keyWindow.frame)
            blackBackgroundView?.backgroundColor = UIColor.black
            blackBackgroundView?.alpha = 0
            
            keyWindow.addSubview(blackBackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackBackgroundView?.alpha = 1
                
                let height = self.startingFrame!.height / self.startingFrame!.width * keyWindow.frame.width
                
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.center = keyWindow.center
                
            }, completion: { (completed) in } )
        }
    } // --- end: увеличиваем картинку в чате при нажатии
    
    
    
    
    
    func handleZoomOut (tapGesture: UITapGestureRecognizer) { // --- start: возращаемся на экран чата после увеличения картинки
        if let zoomingOutImageView = tapGesture.view {
            
            zoomingOutImageView.layer.cornerRadius = 16
            zoomingOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                zoomingOutImageView.frame = self.startingFrame!
                self.blackBackgroundView?.alpha = 0
                
            }, completion: { (completed) in
                
                zoomingOutImageView.removeFromSuperview()
                self.startingImageView?.isHidden = false
                
            })
        }
        
    } // --- end: возращаемся на экран чата после увеличения картинки

    
    
}
