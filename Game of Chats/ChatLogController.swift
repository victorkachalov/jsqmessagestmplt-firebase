//
//  ChatLogController.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 20/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase
import MobileCoreServices
import AVFoundation


class ChatLogController: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let cellId = "cellId"
    
    var user: User? {
        didSet { // --- как только получили значение имени, ставим в заголовок
            navigationItem.title = user?.name
            observeMessages()
        } // ---
    }
    
    var messages = [Message]()
    
    var containerViewBottomAnchor: NSLayoutConstraint?
    var startingFrame: CGRect?
    var blackBackgroundView: UIView?
    var startingImageView: UIImageView?
    
    
    
    
    
    /******** UI start: создадим контейнер с кнопкой отправить и текстовым полем ********/
                /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    
    
    
    lazy var inputContainerView: ChatInputContainerView = {
        
        let chatInputContainerView = ChatInputContainerView(frame: CGRect(x: 0,
                                                                          y: 0,
                                                                          width: self.view.frame.width,
                                                                          height: 50))
        chatInputContainerView.chatLogController = self
        return chatInputContainerView
    }()
    
    
    
    
    func handleUploadTap() { // --- start: выбор мультимедиа
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        present(imagePickerController, animated: true, completion: nil)
        
    } // --- end: выбор мультимедиа
    
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) { // --- start: ставим фотку в профиль после выбора фотки из галереии
        
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            
            handleVideoSelectedForUrl(url: videoUrl)
            
        } else {
            
            handleImageSelectedForInfo(info: info)
            
        }
        
        // --- обязательно делаем дисмисс после выбора,
        // --- иначе экран с галереей не закроется
        dismiss(animated: true, completion: nil)
        
    } // --- end: ставим фотку в профиль после выбора фотки из галереии
    
    
    
    
    private func handleVideoSelectedForUrl(url: URL) { // start: когда выбрано видео
        
        let filename = UUID().uuidString
        let uploadTask = Storage.storage().reference().child("message_movies").child(filename).putFile(from: url, metadata: nil, completion: { (metadata, error) in
            
            if error != nil {
                print("Failed: ", error)
                return
            }
            
            if let videoUrl = metadata?.downloadURL()?.absoluteString {
                
                if let thumbnailImage = self.thumbnailImageForFileUrl(fileUrl: url) {
                    
                    self.uploadToFirebaseStorageUsingImage(image: thumbnailImage, completion: { (imageUrl) in
                       
                        let properties = ["imageUrl"   : imageUrl,
                                          "imageWidth" : thumbnailImage.size.width,
                                          "imageHeight": thumbnailImage.size.height,
                                          "videoUrl"   : videoUrl] as [String : Any]
                        
                        self.sendMessageWithProperties(properties: properties)
                        
                    })
                }
            }
        })
        
        uploadTask.observe(.progress) { (snapshot) in
            if let completedUnitCount = snapshot.progress?.completedUnitCount {
                self.navigationItem.title = String(completedUnitCount)
            }
        }
        
        uploadTask.observe(.success) { (snapshot) in
            self.navigationItem.title = self.user?.name
        }
    } // end: когда выбрано видео
    
    
    
    
    
    private func thumbnailImageForFileUrl(fileUrl: URL) -> UIImage? { //--- start: making the thumbnail from the video
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            
            let thumbnailCGImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60), actualTime: nil)
            return UIImage(cgImage: thumbnailCGImage)
            
        } catch let err {
            
            print(err)
            
        }
        
        return nil
    }//--- end: making the thumbnail from the video

    
    
    
    
    private func handleImageSelectedForInfo(info: [String: Any]) { //---старт: выбор картинки из галереии
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            print("edited image")
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            print("orirginal image")
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            print("image chosen")
            uploadToFirebaseStorageUsingImage(image: selectedImage, completion: { (imageUrl) in
                self.sendMessageWithImageUrl(imageUrl: imageUrl, image: selectedImage)
            })
        }
        
    } //---конец: выбор картинки из галереии
    
    
    
    
    
    private func uploadToFirebaseStorageUsingImage(image: UIImage, completion: @escaping (_ imageUrl: String) -> ()) { //--- start: uploading images to FB
        let imageName = UUID().uuidString
        let ref = Storage.storage().reference().child("message_images").child(imageName)
        
        if let uploadData = UIImageJPEGRepresentation(image, 0.5) as? Data {
            
            ref.putData(uploadData, metadata: nil, completion: { (metadata, error) in
                
                if error != nil {
                    print("Failed to upload the image", error)
                    return
                    
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    
                    completion(imageUrl)
                    
                }
                
            })
        }
        
    }//--- end: uploading images to FB
    
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) { // --- start: когда передумали выбирать мультимедиа
        
        dismiss(animated: true, completion: nil)
        
    } // --- end: когда передумали выбирать мультимедиа
    
    
    
    
    override var inputAccessoryView: UIView? {
        get {
            return inputContainerView
        }
    }
    
    
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    /********* UI end: создадим контейнер с кнопкой отправить и текстовым полем *********/
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        //        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: cellId)
    
        collectionView?.keyboardDismissMode = .interactive
        
        setupKeyboardObservers()

    }
    
    
    
    
    
    
    func observeMessages() { // ---  старт: получим сообщения чтобы  потом наполнять collection view
        
        guard let uid = Auth.auth().currentUser?.uid, let toId = user?.id else {
            return
        }
        
        let userMessagesRef = Database.database().reference().child("user-messages").child(uid).child(toId)
        
        userMessagesRef.observe(.childAdded, with: { (snapshot) in
            
            let messageId = snapshot.key
            let messageRef = Database.database().reference().child("messages").child(messageId)
            
            messageRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                guard let dictionary = snapshot.value as? [String: Any] else {
                    return
                }
                
                let message = Message(dictionary: dictionary)
                
                //потенциальный краш если ключи не совпадают
                self.messages.append(message)
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    // перейти к последнему индексу scrollView
                    let indexPath = IndexPath(item: self.messages.count - 1, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
                }

            }, withCancel: nil)
            
        }, withCancel: nil)
        
    } // --- конец сетевой функции
    
    
    
    
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    /*   working with keyboard. Changing layouts when keyboard is shown or hide    */
    //   методы исполняются так как класс подписан на протокол UITextFieldDelegate //
    
    
    func setupKeyboardObservers() { // старт: ---  ставим слушатели на клавиатуру
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
    } // конец: ---  ставим слушатели на клавиатуру
    
    
    
    
    
    func handleKeyboardDidShow() {
        if messages.count > 0 {
            let indexPath = IndexPath(item: messages.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    
    
    
    func handleKeyboardWillShow(notification: NSNotification) { // старт: --- показываем клавиатуру
        if let userInfo = notification.userInfo {
            if let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                
                
                let keyboardDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSValue)
                containerViewBottomAnchor?.constant = -keyboardFrame.height
                
                if let keyboardDurationTimeInterval = keyboardDuration as? TimeInterval {
                    UIView.animate(withDuration: keyboardDurationTimeInterval, animations: {
                        self.view.layoutIfNeeded()
                    })
                }
            }
        }
    } // конец: --- показываем клавиатуру
    
    
    
    
    
    func handleKeyboardWillHide(notification: NSNotification) { // старт: --- скрываем клавиатуру

        if let userInfo = notification.userInfo {
            if let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
               
                
                let keyboardDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSValue)
                containerViewBottomAnchor?.constant = -keyboardFrame.height
                
                if let keyboardDurationTimeInterval = keyboardDuration as? TimeInterval {
                    containerViewBottomAnchor?.constant = 0
                    UIView.animate(withDuration: keyboardDurationTimeInterval, animations: {
                        self.view.layoutIfNeeded()
                    })
                }
            }
        }
    } // конец: --- скрываем клавиатуру
    
    
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
    } // ==== при переходе на другой экран, например
    
    
    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    
    
    
    
    
    //--- делаем для нормального отображения при повороте экрана ↓
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        collectionView?.collectionViewLayout.invalidateLayout()
        
    }// --- конец ---------------------------------------------- ↑
    
    
    
    
    
    /******** start: работаем с collectionView (population) ********/
       /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatMessageCell
        
        cell.chatLogController = self
        
        let message = messages[indexPath.item]
        
        cell.message = message
    
        cell.textView.text = message.text

        setupCell(cell: cell, message: message)
        
        if let text = message.text {
            cell.bubbleWidthAnchor?.constant = estimateFrameForText(text: text).width + 32
            cell.textView.isHidden = false
        } else if message.imageUrl != nil {
            cell.bubbleWidthAnchor?.constant = 200
            cell.textView.isHidden = true
        }
        
        cell.playButton.isHidden = message.videoUrl == nil
        
        return cell
    }
    
    
    
    
    
    private func setupCell(cell: ChatMessageCell, message: Message) { // --- старт: настройка сообщений (цвет текста, цвет баббл)
        
        if let profileImageUrl = self.user?.profileImageURL {

            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        
        if message.fromId == Auth.auth().currentUser?.uid {
            
            //outgoing msgs (blue+whit)
            
            cell.bubbleView.backgroundColor = ChatMessageCell.blueColor
            cell.textView.textColor = UIColor.white
            
            cell.bubbleViewRightAnchor?.isActive = true
            cell.bubbleViewLeftAnchor?.isActive  = false
            cell.profileImageView.isHidden       = true
        } else {
            
            //incoming msgs (gray+black)
            
            cell.bubbleView.backgroundColor = UIColor(red: 240/255,
                                                      green: 240/255,
                                                      blue: 240/255,
                                                      alpha: 1)
            cell.textView.textColor = UIColor.black
            
            cell.bubbleViewRightAnchor?.isActive = false
            cell.bubbleViewLeftAnchor?.isActive  = true
            cell.profileImageView.isHidden       = false
        }
        
        if let messageImageUrl = message.imageUrl {
            cell.messageImageView.loadImageUsingCacheWithUrlString(urlString: messageImageUrl)
            cell.messageImageView.isHidden = false
            cell.bubbleView.backgroundColor = UIColor.clear
        } else {
            cell.messageImageView.isHidden = true
        }
        
    } // --- конец: настройка сообщений (цвет текста, цвет баббл)
    
    
    
    
    
    private func estimateFrameForText(text: String) -> CGRect { // start: --- зададим размер bubble
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16)], context: nil)
    } // end:  --- зададим размер bubble (аккурат под содержимое текста)
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]
        if let text = message.text {
            height = estimateFrameForText(text: text).height + 20
        } else if let imageWidth = message.imageWidth?.floatValue, let imageHeight = message.imageHeight?.floatValue {
            height = CGFloat(imageHeight / imageWidth * 200)
        }
        
        let width = UIScreen.main.bounds.width
        return CGSize(width: width, height: height)
    }
    
    
    /******** end: работаем с collectionView (population) ********/
      /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    
    
    
    
    
    
    func handleSend() { //-- start:  отправка сообщения пользователю
        
        let properties = ["text": inputContainerView.inputTextField.text!] as [String : Any]
        sendMessageWithProperties(properties: properties)

        inputContainerView.inputTextField.text = ""

    } //-- end:  отправка сообщения пользователю
    
    
    
    
    
    private func sendMessageWithImageUrl(imageUrl: String, image: UIImage) {
        
        let properties = ["imageUrl"   : imageUrl,
                          "imageWidth" : image.size.width,
                          "imageHeight": image.size.height] as [String : Any]
        
        sendMessageWithProperties(properties: properties)

    }
    
    
    
    
    private func sendMessageWithProperties(properties: [String: Any]) {
        
        let ref = Database.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        let toId = user!.id!
        let fromId = Auth.auth().currentUser!.uid
        let timestamp = UInt64(Date().timeIntervalSince1970)
        
        var values = ["toId"       : toId,
                      "fromId"     : fromId,
                      "timestamp"  : timestamp] as [String : Any]
        
        properties.forEach {values[$0] = $1}
        
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print(error)
                return
            }
            
            let userMessageRef = Database.database().reference().child("user-messages").child(fromId).child(toId)
            let messageId = childRef.key
            userMessageRef.updateChildValues([messageId: 1])
            
            let recipientUserMessagesRef = Database.database().reference().child("user-messages").child(toId).child(fromId)
            recipientUserMessagesRef.updateChildValues([messageId: 1])
        }
    }
    
    

    
    
    func performZoomInForStartingImageView (startingImageView: UIImageView) { // --- start: увеличиваем картинку в чате при нажатии
        
        self.startingImageView = startingImageView
        startingFrame = startingImageView.superview?.convert(startingImageView.frame, to: nil)
        
        let zoomingImageView = UIImageView(frame: startingFrame!)
        zoomingImageView.image = startingImageView.image
        zoomingImageView.isUserInteractionEnabled = true
        
        zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
        
        if let keyWindow = UIApplication.shared.keyWindow {
            
            blackBackgroundView = UIView(frame: keyWindow.frame)
            blackBackgroundView?.backgroundColor = UIColor.black
            blackBackgroundView?.alpha = 0
            
            keyWindow.addSubview(blackBackgroundView!)
            keyWindow.addSubview(zoomingImageView)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackBackgroundView?.alpha = 1
                self.inputContainerView.alpha = 0
                
                let height = self.startingFrame!.height / self.startingFrame!.width * keyWindow.frame.width
                
                zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
                zoomingImageView.center = keyWindow.center
                
            }, completion: { (completed) in } )
        }
    } // --- end: увеличиваем картинку в чате при нажатии
    
    
    
    
    
    func handleZoomOut (tapGesture: UITapGestureRecognizer) { // --- start: возращаемся на экран чата после увеличения картинки
        if let zoomingOutImageView = tapGesture.view {
            
            zoomingOutImageView.layer.cornerRadius = 16
            zoomingOutImageView.clipsToBounds = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                    zoomingOutImageView.frame = self.startingFrame!
                    self.blackBackgroundView?.alpha = 0
                    self.inputContainerView.alpha   = 1
                
            }, completion: { (completed) in
                
                    zoomingOutImageView.removeFromSuperview()
                    self.startingImageView?.isHidden = false
                
            })
        }
        
    } // --- end: возращаемся на экран чата после увеличения картинки
    
    
}
