//
//  ViewController.swift
//  Game of Chats
//
//  Created by Victor Kachalov on 15.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase
import SnapKit

class MessagesController: UITableViewController {

    
    var messages = [Message]()
    var messageDictionary = [String: Message]()
    
    let cellId = "cell"
    
    var timer: Timer?
    
    let messagesListView: MessagesListView = {
        let messageController = MessagesListView()
        return messageController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //--- устанавливаем в NavigationBar кнопку Logout
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(handleLogout))
        
        //--- устанавливаем в NavigationBar кнопку New message
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "msg.png"),
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(handleNewMessage))
        
        
        checkIfUserIsLoggedIn()
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        tableView.allowsSelectionDuringEditing = true
        
    }
    
    
    
    
    
    func checkIfUserIsLoggedIn () { // start: --- проверка юзера на авторизацию
        
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
            //handleLogout()
        } else if UserDefaults.standard.string(forKey: "customToken") != nil {
            self.fetchUserAndSetupNavBarTitle()
            print(UserDefaults.standard.string(forKey: "customToken"))
        }
        
    } //  end: --- проверка юзера на авторизацию
    
    
    
    
    
    func fetchUserAndSetupNavBarTitle() { // start: --- установка имени юзера на НавБар
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { snapshot in
            
            if let dictionary = snapshot.value as? [String: Any] {
                
                let user = User()
                user.setValuesForKeys(dictionary)
                self.setupNavBarWithUser(user: user)
                
            }
            
        }, withCancel: nil)

    } // end: --- установка имени юзера на НавБар
    
    
    
   
    
    func setupNavBarWithUser(user: User) { // --- start: будем устанавливать список чатов для юзера (+ установка View аттрибутов)
        
        messages.removeAll()
        messageDictionary.removeAll()
        tableView.reloadData()
        
        observeUserMessages()
        
        messagesListView.titleView.frame = CGRect(x     : 0,
                                                  y     : 0,
                                                  width : 100,
                                                  height: 40)
        
        if let profileImageUrl = user.profileImageURL {
            messagesListView.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        
        self.navigationItem.titleView = messagesListView.titleView
        
    } // --- end: будем устанавливать список чатов для юзера (+ установка View аттрибутов)
    

    
    
    
    func showChatControllerForUser(user: User) { // --- start: переход на chatLogController
        
//        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
//        
//        chatLogController.user = user
//        
//        // ------- аналог сегвея через навБар ----------------
//        navigationController?.pushViewController(chatLogController, animated: true)
        
        let jsq = JSQChatLogController()
        jsq.user = user
        navigationController?.pushViewController(jsq, animated: true)
        
    } // --- end: переход на chatLogController
    
    
    
    
    
    func handleLogout() { // start: --- выполнение при нажатии Logout
        
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let loginController = LoginController()
        loginController.messagesController = self
        
        /* переходим на экран логин/регистрация */
        // ------- аналог сегвея ----------------
        present(loginController, animated: true, completion: nil)
    
    }// end: --- выполнение при нажатии Logout
    
    
    
    
    
    func observeUserMessages() { // --- начало: наполняем чатами нашего залогиненного юзера
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let ref = Database.database().reference().child("user-messages").child(uid)
        
        ref.observe(.childAdded, with: { (snapshot) in
            
            let userId = snapshot.key // --- по ключу мы узнаем айдишник
            Database.database().reference().child("user-messages").child(uid).child(userId).observe(.childAdded, with: { (snapshot) in
                
                let messageId = snapshot.key
                self.fetchMessageWithMessageId(messageId: messageId)
                
            }, withCancel: nil)
            
        }, withCancel: nil)
        
        ref.observe(.childRemoved, with: { (snapshot) in
            
            self.messageDictionary.removeValue(forKey: snapshot.key)
            self.attemptReloafOfTable()
            
        }, withCancel: nil)
        
    } // --- конец: наполняем чатами нашего залогиненного юзера
    
    
    
    
    
    private func fetchMessageWithMessageId (messageId: String) { // старт: --- получаем сообщением по айди
        
        let messageReference = Database.database().reference().child("messages").child(messageId)
        
        messageReference.observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if let dictionary = snapshot.value as? [String: Any] {
                
                let message = Message(dictionary: dictionary)
                
                if let chatPartnerId = message.chatPartnerId() { // --- наполняем значениями +
                    
                    self.messageDictionary[chatPartnerId] = message
                    
                } // --- наполняем значениями -
                
                //проверка что timestamp все таки UInt64
                if let timestamp = message.timestamp as? UInt64 {
                    
                }//--- работает
                
                self.attemptReloafOfTable()
            }
            
        }, withCancel: nil)
        
    } // конец: --- получаем сообщением по айди
    
    
    
    
    
    private func attemptReloafOfTable() {
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    
    
    
    
    func handleReloadTable() { // начало: ---  обновляем таблицу
        
        self.messages = Array(self.messageDictionary.values)
        self.messages.sort(by: { (message1, message2) -> Bool in
            
            return (message1.timestamp?.intValue)! > (message2.timestamp?.intValue)!
            
        })
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    } // конец: ---  обновляем таблицу
    
    
    
    

    func handleNewMessage() { // --- start: go to the NewMessageVC
        
        let newMessageController = NewMessageController()
        newMessageController.messagesController = self
        let navController = UINavigationController(rootViewController: newMessageController)
        
        present(navController, animated: true, completion: nil)
        
    } // --- end: go to the NewMessageVC
    
    
    
    
    //--------------  Начало: настройка таблицы -------------//
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        let message = messages[indexPath.row]
        cell.message = message
        return cell
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let message = messages[indexPath.row]
        
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        
        let ref = Database.database().reference().child("users").child(chatPartnerId)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else {
                return
            }
            
            let user = User()
            user.id = chatPartnerId
            user.setValuesForKeys(dictionary)
            
            self.showChatControllerForUser(user: user)
            
        }, withCancel: nil)
        
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        let message = self.messages[indexPath.row]
        
        if let chatPartnerId = message.chatPartnerId() {
            Database.database().reference().child("user-messages").child(uid).child(chatPartnerId).removeValue(completionBlock: { (error, ref) in
                
                if error != nil {
                    print(error)
                }
                
                self.messageDictionary.removeValue(forKey: chatPartnerId)
                self.attemptReloafOfTable()
                
            })
        }
        
        
    }

    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    //----------------  Конец: настройка таблицы -------------//
    
}

