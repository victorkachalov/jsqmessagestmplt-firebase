//
//  MessagesListView.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 29/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import SnapKit

class MessagesListView: UIView {
    
    let titleView: UIView = {
        let titleView = UIView()
        titleView.frame = CGRect(x     : 0,
                                 y     : 0,
                                 width : 100,
                                 height: 40)
        return titleView
    }()
    
    let nameLabel = UILabel()
    
    let containerView = UIView()
    
    let profileImageView: UIImageView = {
        let profileImageView = UIImageView()
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        return profileImageView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
   
        titleView.addSubview(containerView)
        containerView.addSubview(profileImageView)
        containerView.addSubview(nameLabel)

        
        profileImageView.snp.makeConstraints { (make) in
            make.left.equalTo(containerView.snp.left)
            make.centerY.equalTo(containerView.snp.centerY)
            make.width.height.equalTo(40)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(profileImageView.snp.right).offset(10)
            make.centerY.equalTo(profileImageView.snp.centerY)
            make.right.equalTo(containerView.snp.right)
            make.height.equalTo(profileImageView.snp.height)
        }
        
        containerView.snp.makeConstraints { (make) in
            make.center.equalTo(titleView.snp.center)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
