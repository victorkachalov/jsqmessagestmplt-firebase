//
//  Extensions.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 20/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import SDWebImage

/* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
// ------ РАБОТА С СОХРАНЕНИМ В КЭШ КАРТИНОК-АВАТАРОК ---//



extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String) { // --- start: записываем картинки в кэш
     
        self.sd_setImage(with: URL(string: urlString), placeholderImage: self.image, options: .continueInBackground, completed: nil)

    } // --- end: записываем картинки в кэш
    
}


/* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
// ------ РАБОТА С СОХРАНЕНИМ В КЭШ КАРТИНОК-АВАТАРОК ---//



