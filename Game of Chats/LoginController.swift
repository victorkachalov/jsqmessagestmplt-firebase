//
//  LoginController.swift
//  Game of Chats
//
//  Created by Victor Kachalov on 15.09.17.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit
import Firebase

class LoginController: UIViewController {
    
    var messagesController: MessagesController?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { // --- меняем цвет статус бара
        return .lightContent
    } // end --- меняем цвет статус бара
    
    /* MARK: start: ------------- наполяем (populating) LoginController сабвьюхами --------------- */
    /* будем делать через самовызываемые безымянные функции (immediatly invocated function blocks) */
    
    let inputsContainerView: UIView = { //--- start: создадим контейнер с элементами inputsContainerView
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }() //---  end: создадим контейнер с элементами inputsContainerView
    
    lazy var loginRegisterButton : UIButton = { //--- start: создадим кнопку Register
        let button = UIButton(type: UIButtonType.system)
        button.backgroundColor = UIColor(red: 80/255,
                                         green: 101/255,
                                         blue: 161/255,
                                         alpha: 1)
        button.setTitle("Register", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
        button.addTarget(self, action: #selector(handleLoginRegister), for: UIControlEvents.touchUpInside)
        
        return button
    }() //--- end: создадим кнопку Register
    
    let nameTextField: UITextField = { //--- start: создадим textField name
        let textField = UITextField()
        textField.placeholder = "Name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }() //--- end: создадим textField name
    
    let nameSeparatorView: UIView = { //--- start: создадим разделитель между emailTextField and nameTextField
        let view = UIView()
        view.backgroundColor = UIColor(red: 220/255,
                                       green: 220/255,
                                       blue: 220/255,
                                       alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }() //--- end: создадим разделитель между emailTextField and nameTextField
    
    
    let emailTextField: UITextField = { //--- start: создадим textField email
        let textField = UITextField()
        textField.placeholder = "Email"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }() //--- end: создадим textField email
    
    // создадим разделитель между passwordTextField and emailTextField
    let emailSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 220/255,
                                       green: 220/255,
                                       blue: 220/255,
                                       alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let passwordTextField: UITextField = { //--- start: создадим textField password
        let textField = UITextField()
        textField.placeholder = "Password"
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.isSecureTextEntry = true
        return textField
    }() //--- end: создадим textField password
    
    lazy var profileImageView: UIImageView = { //--- start: создадим profileImageView
        let imageView = UIImageView()
        imageView.image = UIImage(named: "stark")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImageView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }() //--- end: создадим profileImageView
    
    
    
    lazy var loginRegisterSegmentControl: UISegmentedControl = { //--- start: создадим loginRegisterSegmentControl
        let segmentControll = UISegmentedControl(items: ["Login", "Register"])
        segmentControll.translatesAutoresizingMaskIntoConstraints = false
        segmentControll.tintColor = UIColor.white
        segmentControll.selectedSegmentIndex = 1
        
        segmentControll.addTarget(self, action: #selector(handleLoginRegisterChange), for: UIControlEvents.valueChanged)
        
        return segmentControll
    }() //--- end: создадим loginRegisterSegmentControl
    
    /* end: ---------------- наполяем (populating) LoginController сабвьюхами ------------------ */


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // --- задаем цвет бэкграунда для LoginController
        view.backgroundColor = UIColor(red: 61/255, green: 91/255, blue: 151/255, alpha: 1)
        
        // --- добавляем наши элементы на вью
        view.addSubview(inputsContainerView)
        view.addSubview(loginRegisterButton)
        view.addSubview(nameTextField)
        view.addSubview(nameSeparatorView)
        view.addSubview(emailTextField)
        view.addSubview(emailSeparatorView)
        view.addSubview(passwordTextField)
        view.addSubview(profileImageView)
        view.addSubview(loginRegisterSegmentControl)
        
        // --- исполняем установку constraints
        setupInputsContainerView()
        setupLoginRegisterButton()
        setupProfileImage()
        setupLoginRegisterSegmentControl()
        
    }
    
    /* ===================== MARK: START: setup the Constraints for Views ======================== */
    /* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
    
    var inputsContainerViewHeigtAnchor : NSLayoutConstraint?
    var nameTextFieldHeightAnchor      : NSLayoutConstraint?
    var nameSeparatorViewHeightAnchor  : NSLayoutConstraint?
    var emailTextFieldHeightAnchor     : NSLayoutConstraint?
    var passwordTextFieldHeightAnchor  : NSLayoutConstraint?
    
    func setupInputsContainerView() { // --- установим constraints для inputsContainerView и для всех вью в него входящих
        
        // inputsContainerView: потребуется х, у, ширина и высота
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive            = true
        inputsContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive            = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        inputsContainerViewHeigtAnchor = inputsContainerView.heightAnchor.constraint(equalToConstant: 150)
        inputsContainerViewHeigtAnchor?.isActive = true
        
        // nameTextField: потребуется х, у, ширина и высота
        nameTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
        nameTextField.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive                 = true
        nameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive             = true
        nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor,
                                                                          multiplier: 1/3)
        nameTextFieldHeightAnchor?.isActive = true
        
        // nameSeparatorView: потребуется х, у, ширина и высота
        nameSeparatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive   = true
        nameSeparatorView.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive        = true
        nameSeparatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        nameSeparatorViewHeightAnchor = nameSeparatorView.heightAnchor.constraint(equalToConstant: 1)
        nameSeparatorViewHeightAnchor?.isActive = true
        
        // emailTextField: потребуется х, у, ширина и высота
        emailTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
        emailTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive                    = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive             = true
        emailTextFieldHeightAnchor =  emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor,
                                                                             multiplier: 1/3)
        emailTextFieldHeightAnchor?.isActive = true
        
        // emailSeparatorView: потребуется х, у, ширина и высота
        emailSeparatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive   = true
        emailSeparatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive       = true
        emailSeparatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive                      = true
        
        // passwordTextField: потребуется х, у, ширина и высота
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor, constant: 12).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive                   = true
        passwordTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive             = true
        passwordTextFieldHeightAnchor = passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor,
                                                                                  multiplier: 1/3)
        passwordTextFieldHeightAnchor?.isActive = true
        
        
    } //end: --- установим constraints для inputsContainerView и для всех вью в него входящих
    
    func setupLoginRegisterButton() { // --- установим constraints для loginRegisterButton
        
        // потребуется х, у, ширина и высота
        loginRegisterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive                         = true
        loginRegisterButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 12).isActive = true
        loginRegisterButton.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive              = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive                                  = true
        
    } //end: --- установим constraints для loginRegisterButton
    
    func setupProfileImage() { // --- установим constraints для profileImageView
        
        // потребуется х, у, ширина и высота
        profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive                                  = true
        profileImageView.bottomAnchor.constraint(equalTo: loginRegisterSegmentControl.topAnchor, constant: -12).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 150).isActive                                           = true
        profileImageView.heightAnchor.constraint(equalToConstant: 150).isActive                                          = true
        
    } // end: --- установим constraints для profileImageView
    
    func setupLoginRegisterSegmentControl() { // --- установим constraints для setupLoginRegisterSegmentControl
        
        // loginRegisterSegmentControl: потребуется х, у, ширина и высота
        
        loginRegisterSegmentControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive                          = true
        loginRegisterSegmentControl.bottomAnchor.constraint(equalTo: inputsContainerView.topAnchor, constant: -12).isActive = true
        loginRegisterSegmentControl.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive               = true
        loginRegisterSegmentControl.heightAnchor.constraint(equalToConstant: 35).isActive                                   = true
        
        
    } // end: --- установим constraints для setupLoginRegisterSegmentControl
    
    func handleLoginRegisterChange() {
        let title = loginRegisterSegmentControl.selectedSegmentIndex
        loginRegisterButton.setTitle(loginRegisterSegmentControl.titleForSegment(at: title), for: .normal)
        
        // изменить высоту блока с именем/емейл/пароль
        inputsContainerViewHeigtAnchor?.constant = loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 100 : 150
        
        // изменить высоту поля ввода имени
        nameTextFieldHeightAnchor?.isActive = false
        nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor,
                                                                          multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 0 : 1/3)
        nameTextFieldHeightAnchor?.isActive = true
        
        if loginRegisterSegmentControl.selectedSegmentIndex == 0 {
            nameTextField.placeholder = nil
            nameTextField.text = nil
        } else {
            nameTextField.placeholder = "Name"
        }
        
        nameSeparatorViewHeightAnchor?.isActive = false
        nameSeparatorViewHeightAnchor = nameSeparatorView.heightAnchor.constraint(equalToConstant: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 0 : 1)
        nameSeparatorViewHeightAnchor?.isActive = true
        
        // изменить высоту поля ввода email
        emailTextFieldHeightAnchor?.isActive = false
        emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor,
                                                                            multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        emailTextFieldHeightAnchor?.isActive = true
        
        // изменить высоту поля ввода password
        passwordTextFieldHeightAnchor?.isActive = false
        passwordTextFieldHeightAnchor = passwordTextField.heightAnchor.constraint(equalTo: inputsContainerView.heightAnchor,
                                                                                  multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        passwordTextFieldHeightAnchor?.isActive = true
    }
    
    /* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
    /* =====================  END: setup the Constraints for Views =============================== */
    
}
