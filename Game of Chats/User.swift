//
//  User.swift
//  Game of Chats
//
//  Created by iOS_Razrab on 19/09/2017.
//  Copyright © 2017 vk. All rights reserved.
//

import UIKit


/* ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ */
// ----------- Создаем класс Пользователь ---------------//
// -- в объекты класса будем передавать данные полученны //
// ------------------ из Firebase -----------------------//



class User: NSObject {
    var id              : String?
    var name            : String?
    var email           : String?
    var profileImageURL : String?
    
}



/* ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ */
